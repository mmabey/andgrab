# Andgrab
-----

Andgrab is a simple forensic tool for extracting data off an Android phone. It's quite old and was written around Spring 2011. It also has a number of shortcomings that I was never able to work out (some of which I've documented by creating issues). Having said that, the code and the approach may be of use to someone, so I decided to make it available.
