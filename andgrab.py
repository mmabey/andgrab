#! /bin/usr/python
# -*- coding: utf-8 -*-

import subprocess
import os
import sys
import re
import time
import shlex
import shutil
from checksum import checksum

DEBUG = False
VERBOSE = True
GZIP = False
WIN7 = True  # Only has effect if the system is win32

SKIP_PARTS = False
OMIT_PARTS = False
PARTS = ('userdata')
SHALLOW_FILTER = True


class AndGrab:
    # Exceptions
    class DataRetrievalError(Exception):
        pass

    class ChecksumError(Exception):
        pass

    class MaxDepthReached(Exception):
        pass

    def __init__(self):
        if sys.platform == "win32":
            if DEBUG:
                dResp = raw_input("Setting environment to Windows. Okay? ")
                if dResp != '':
                    os._exit(0)
            # Change Windows's stdout mode to binary
            import msvcrt
            msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
            # Set Windows specific paths
            if WIN7:
                self.adb_path = r"C:\Users\Mike\Desktop\android-sdk-windows\platform-tools"
                self.outputPath = r"C:\Users\Mike\workspace\work\yaffs\images"
            else:
                self.adb_path = r"C:\Documents and Settings\mmabey\Desktop\android-sdk-windows\tools"
                self.outputPath = r"C:\Documents and Settings\mmabey\workspace\all\yaffs\images"
            self.cmd = [os.path.join(self.adb_path, 'adb')]
        else:  # Assume running on Linux
            if DEBUG:
                dResp = raw_input("Setting environment to Linux. Okay? ")
                if dResp != '':
                    os._exit(0)
            # Set Linux paths
            self.adb_path = r"/home/mmabey/android-sdk-linux_x86/tools"
            self.outputPath = r"/home/mmabey/forensics/images"
            self.cmd = [os.path.join(self.adb_path, 'adb')]
        self.dev = []
        self.CheckAndDevs(self.SelectDevice)
        self.validFSs, self.validVirtFSs = self.GetValidFSs()

    def ExecuteADB(self, command, extra=[], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE):
        args = self.cmd+self.dev+['shell', 'su', '-c']+('"'+command+'"').split()
        pr = subprocess.Popen(args, stdin=stdin, stdout=stdout, stderr=stderr)
        return pr.communicate()

    def Execute(self, command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE):
        pr = subprocess.Popen(shlex.split(command), stdin=stdin, stdout=stdout, stderr=stderr)
        return pr.communicate()

    # Entry point function
    def GrabData(self, cs=True, gz=False, timer=False):
        """
        Executes the necessary commands to pull the selected data off the
        device.

        Note that the checksum variables are lists so they can contain the
        results from all of the algorithms passed to the checksum functions
        CalcPreSums and CalcPostSums.
        """
        global GZIP
        GZIP = bool(gz)

        if VERBOSE and SHALLOW_FILTER:
            print "NOTICE: SHALLOW_FILTER is on. Filtering will only be retried once."
        if VERBOSE and timer:
            print "NOTICE: The execution will be timed and printed upon completion."

        # Start timer, if desired
        if timer:
            # t0 = time.clock() # Supposed to be based on CPU cycles
            t0 = time.time()

        for (name, path, info) in self.GetAcqInfo():
            if VERBOSE:
                print
                print "Now working on %s" % name
                print
            if SKIP_PARTS and name not in PARTS:
                print "Skipping partition: %s" % name
                continue
            if OMIT_PARTS and name in PARTS:
                print "Omitting partition: %s" % name
                continue
            if cs:
                presums = self.CalcPreSums(path, name)
            fname = self.Acquire(path, name)
            if cs:
                postsums = self.CalcPostSums(name)
                try:
                    if SHALLOW_FILTER:
                        self.VerifySums(presums, postsums, fname, filter=True, maxdepth=1)
                    else:
                        self.VerifySums(presums, postsums, fname, filter=True)
                except self.ChecksumError, e:
                    # Failed to get a perfect copy. Inform the user of this and go on
                    # with the next acquisition.
                    if VERBOSE:
                        print e
            with open(os.path.join(self.outputPath, self.dev[1], name+"_info.txt"), 'w') as finfo:
                finfo.write(info[0])

        # Stop the timer
        if timer:
            # t = time.clock() - t0
            t = time.time() - t0
            if VERBOSE:
                print "\n\nTime taken to acquire images: %s" % self.FormatTime(t)

    def FormatTime(self, t):
        timeStr = ''

        if t < 60:
            timeStr = '%.2f seconds' % t
        else:
            s = t % 60
            timeStr = '%.2f seconds' % s
            m = t = int(t)/60
            if t >= 60:
                m = t % 60
                h = int(t)/60
                timeStr = '%d hours, %d minutes, and %s' % (h, m, timeStr)
            else:
                timeStr = '%d minutes and %s' % (m, timeStr)

        return timeStr

    def Acquire(self, path, name):
        global GZIP
        fpath = os.path.join(self.outputPath, self.dev[1])
        if not os.access(fpath, os.F_OK):
            os.mkdir(fpath)
        fname = os.path.join(fpath, name+".dd")
        gz = ''
        if DEBUG:
            print "Value of GZIP:", GZIP
        if GZIP:
            gz = ' | gzip'
            fname += '.gz'
        if DEBUG:
            dResp = raw_input("Executing dd on "+path+" and piping results to "+fname+". Okay? ")
            if dResp != '':
                os._exit(0)
        elif VERBOSE:
            print
            print "Acquiring from: "+path
            print "Storing at    : "+fname
            print
        fout = open(fname, 'wb')

        self.ExecuteADB('dd if='+path+' 2>/dev/null'+gz, stdout=fout)
        fout.close()
        return fname

    def CalcPreSums(self, devfile, name, sums=('md5', 'sha1'), bbMode=False):
        """
        Uses the device's md5sum and sha1sum utilities to calculate the
        checksum of the file before transmitting it via a piped dd.
        """
        if DEBUG:
            dResp = raw_input("Calculating pre-checksum for "+name+". Okay? ")
            if dResp != '':
                os._exit(0)
        if VERBOSE and not bbMode:
            print "Calculating checksum on the device..."
        sumVals = []

        b = ''
        if bbMode:
            b = 'busybox '

        for s in sums:
            fpath = os.path.join(self.outputPath, self.dev[1])
            if not os.access(fpath, os.F_OK):
                os.mkdir(fpath)
            fout = open(os.path.join(fpath, name+s+'sum.'+s), 'w')
            result = self.ExecuteADB(b+s+'sum '+devfile)
            # Isolate text if no errors ocurred
            if result[1] == '':
                result = result[0]
            else:
                raise self.DataRetrievalError(result[1])
            if "Permission denied" in result:
                if DEBUG:
                    print "Switching to busybox mode..."
                return self.CalcPreSums(devfile, name, sums, True)
            fout.write(result.split()[0])
            fout.close()
            sumVals.append(result.split()[0])
        if VERBOSE:
            print "Done"
        return tuple(sumVals)

    def CalcPostSums(self, name, sums=('md5', 'sha1')):
        """
        Uses the local Python library to calculate the checksum of the locally
        saved file.
        """
        if DEBUG:
            dResp = raw_input("Calculating post-checksum for "+name+". Okay? ")
            if dResp != '':
                os._exit(0)
        if VERBOSE:
            print "Calculating checksum here..."
        sumVals = []
        pth = os.path.join(self.outputPath, self.dev[1], name+'.dd')
        for sum in sums:
            if sys.platform == "linux2":
                sumVals.append(self.Execute(sum+"sum "+pth)[0].split()[0])
            else:
                sumVals.append(checksum(pth, sum))
        if VERBOSE:
            print "Done"
        return tuple(sumVals)

    def SelectDevice(self, devs):
        """
        Currently defaults to the first listed device (0).
        """
        while True:
            try:
                devs.remove('')
            except ValueError:
                break

        if len(devs) == 0:
            print "No devices found. Exiting"
            os._exit(0)
        elif len(devs) == 1:
            return 0
        else:  # if False
            print "The following devices are connected via ADB:"
            print
            for d in range(len(devs)):
                print d+1, ':', devs[d]
            s = 0
            while ((int(s)-1) not in range(len(devs))):
                s = raw_input("Please enter a number 1-"+str(len(devs))+": ")
            return int(s)-1
        return 0

    def GetAcqInfo(self):
        mtdPaths = self.ConvertToMTDPaths()
        partInfo = self.GetYAFFSPartInfo()
        result = []
        for mtd, name in self.GetMTDInfo():
            try:
                i = mtdPaths.index('/dev/mtd/'+mtd)
            except ValueError:
                continue
            else:
                for j in range(len(partInfo)):
                    if name == partInfo[j][1]['Name']:
                        result.append((name, mtdPaths[i]+'ro', partInfo[j]))  # 'ro' = read-only
                        break
        if VERBOSE and False:
            print "\nAcquisition info:\n"
            print result
        return tuple(result)

    def GetYAFFSPartInfo(self):
        """
        Returns a list of info on the partitions on the device as provided by
        the /proc/yaffs utility. The info is in the format: (the string
        printed by yaffs, a dictionary of all the values, a tuple of the
        dictionary keys in the order they appeared in the string).
        """
        if DEBUG:
            dResp = raw_input("Retrieving info from /proc/yaffs. Okay? ")
            if dResp != '':
                os._exit(0)
        info = self.ExecuteADB('cat /proc/yaffs')
        # Isolate text if no errors ocurred
        if info[1] == '':
            info = info[0].replace('\r', '').split('\n\n')[1:]
        else:
            raise self.DataRetrievalError(info[1])

        partInfo = []
        for i in info:
            p = {"Name": i.split('"')[1]}
            k = []  # keys
            for line in i.split('\n')[1:]:
                if line == '':
                    continue
                l = line.split()
                kTmp = l[0].replace('.', '')
                p[kTmp] = l[1]
                k.append(kTmp)
            partInfo.append((i, p, k))
        return partInfo

    def GetMountedDevs(self, fsTypes=['yaffs2', 'yaffs']):
        """
        Executes the 'mounts' utility and parses the output. A list of desired
        file system types can optionally be passed. Returns a list of the
        devices mounted which are of the specified file system types.
        """  # or have a mount path under root
        if DEBUG:
            dResp = raw_input("Retrieving info from /proc/mounts. Okay? ")
            if dResp != '':
                os._exit(0)
        mounts = self.ExecuteADB('cat /proc/mounts')
        # Isolate text if no errors ocurred
        if mounts[1] == '':
            mounts = mounts[0]
        else:
            raise self.DataRetrievalError(mounts[1])
        devs = []

        # Each line in the mounts string has the following data in a space-delimited format:
        # 0-Device mounted
        # 1-Mount point
        # 2-File system type
        # 3-Read-only (ro) or read-write (rw)
        for l in mounts.split('\n'):
            # l might still have two '\r' characters trailing. Remove them. Then split by whitespace.
            s = l.strip().split()
            try:
                if s[2] in self.validFSs:  # s[2] in fsTypes and
                    devs.append(s[0])
                elif False and s[0][0] == '/':  # Device mounted has a path under root
                    devs.append(s[0])
            except IndexError:
                pass
        return devs

    def ConvertToMTDPaths(self, devices=[]):
        """
        Converts block device paths to mtd device paths.
        """
        if devices == []:
            devices = self.GetMountedDevs()
        tmp = []
        for i in range(len(devices)):
            if 'mtdblock' in devices[i]:
                tmp.append(devices[i].replace('block/mtdblock', 'mtd/mtd'))
            else:
                tmp.append(devices[i])
        return tmp

    def GetMTDInfo(self):
        if DEBUG:
            dResp = raw_input("Retrieving info from /proc/mtd. Okay? ")
            if dResp != '':
                os._exit(0)
        mtd = self.ExecuteADB('cat /proc/mtd')
        # Isolate text if no errors ocurred
        if mtd[1] == '':
            mtd = mtd[0]
        else:
            raise self.DataRetrievalError(mtd[1])

        tmp = []
        for line in mtd.replace('\r', '').split('\n'):
            m = re.search(r'(.*):.*"(.*)"', line)
            if m is None:
                continue
            tmp.append((m.group(1), m.group(2)))
        return tuple(tmp)

    def GetValidFSs(self):
        if DEBUG:
            dResp = raw_input("Retrieving info from /proc/filesystems. Okay? ")
            if dResp != '':
                os._exit(0)
        fss = self.ExecuteADB('cat /proc/filesystems')
        # Isolate text if no errors ocurred
        if fss[1] == '':
            fss = fss[0].split()
        else:
            raise self.DataRetrievalError(fss[1])

        vfss = []  # Virtual file systems container
        try:
            while(True):
                i = fss.index('nodev')
                vfss.append(fss[i+1])
                fss.remove(fss[i+1])
                fss.remove('nodev')
        except ValueError:
            # Removed all occurrances
            pass

        return (tuple(fss), tuple(vfss))

    def CheckAndDevs(self, callback):
        """
        Checks to see if more than one device is connected. If so, callback is
        called with a list of connected devices.  The callback function should
        return an integer index which corresponds to the desired device to
        connect to.
        """
        if DEBUG:
            dResp = raw_input("Retrieving info about devices connected to ADB. Okay? ")
            if dResp != '':
                os._exit(0)
        pr = subprocess.Popen(self.cmd+['devices'], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)
        andDevs = pr.communicate()
        # Isolate text if no errors ocurred
        if andDevs[1] == '':
            andDevs = andDevs[0].split('\n')
        else:
            raise self.DataRetrievalError(andDevs[1])

        devSerials = []
        for d in andDevs[1:]:
            d = d.strip()
            if d == '' or d[0] == '*':
                continue
            devSerials.append(d.split()[0])

        if len(devSerials) == 1:
            index = 0
        else:
            index = callback(devSerials)

        # Set the serial number
        self.dev = ['-s', str(devSerials[index])]

    def ChangeDevice(self, callback):
        self.CheckAndDevs(callback)

    def CmdLineGetOutputFilename(self):
        tmp = raw_input("Enter something: ")
        # Check if tmp is a valid dir
        return tmp

    def FilterCarriages(self, fname, pre, post, depth, maxdepth):
        r"""
        Parses the file at fname, removes any instances of the character
        sequence '\r\n', performs a checksum on the new file, and verifies the
        new file is the same as the original
        """
        if DEBUG:
            dResp = raw_input("Filtering. Okay? ")
            if dResp != '':
                os._exit(0)
        # Create temporary file, open image file
        with open(fname+'.new', 'wb') as fout:
            with open(fname, 'rb') as fin:
                for line in fin:
                    fout.write(line.replace('\x0D\x0A', '\x0A'))

        # Use temporary file to replace local image, save original
        os.remove(fname)
        os.rename(fname+'.new', fname)

        # Generate checksum to see if anything actually changed
        newsums = self.CalcPostSums(os.path.splitext(os.path.basename(fname))[0])
        try:
            self.VerifySums(post, newsums)
        except self.DataRetrievalError:
            # Success! Something changed. Proceed with checking integrity
            pass
        else:
            # Fail. Filtering isn't going to work.
            self.checksumError(fname)

        # Check if the file is now the same as the original
        self.VerifySums(pre, newsums, fname, filter=True, depth=depth+1, maxdepth=maxdepth)

    def VerifySums(self, presums, postsums, fname=None, filter=False, depth=0, maxdepth=5):
        # If this is the first time we've come here to filter, make a copy of the original, just in case.
        if depth == 0 and filter:
            shutil.copyfile(fname, fname+'.orig')

        # Checksums matched
        if presums == postsums:
            try:
                os.remove(fname+'.orig')
            except OSError, e:
                if e.errno == 2:
                    pass  # File didn't exist. That's fine.
                else:
                    raise  # Re-raise last exception
            if DEBUG:
                print "\n\nChecksums Matched!!!\n\n"
            return

        # Do not run more times than maxdepth
        elif depth == maxdepth:
            raise self.MaxDepthReached("Reached maximum depth for filtering carriage returns while verifying %s." %
                                       fname)

        # Time to try filtering out carriage returns
        elif filter and presums != postsums:
            if DEBUG:
                print "\nChecksum verification failed. Attempting to filter out carriage returns...\nOriginal: ' \
                    '%s\nNew:      %s\n" % (presums, postsums)
            try:
                self.FilterCarriages(fname, presums, postsums, depth, maxdepth)
            except self.MaxDepthReached, e:
                # Filtering failed because maxdepth was reached. Inform user of this condition and that the current
                # version of the *.dd file is tainted.
                if VERBOSE:
                    print e
                self.checksumError(fname)

        # Not going to filter. Just give up.
        else:
            raise self.DataRetrievalError("Checksums to not match for %s:\nOriginal: %s\nNew:      %s" %
                                          (fname, presums, postsums))

    def checksumError(self, fname):
        msg = ("The file %s has been irreparably tainted. Two versions of this file should be in the output folder. "
               "Use the one named *.dd.orig, but be aware it may have extra carriage return characters in it.") % fname
        raise self.ChecksumError(msg)

if __name__ == '__main__':
    a = AndGrab()
    fout = open("testOut.dd", 'wb')
    a.ExecuteADB('dd if=/init.sholes.rc 2>/dev/null', stdout=fout)
    fout.close()
